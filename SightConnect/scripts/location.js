(function (global) {
    var map,
		geocoder,
        myLocation,
		isLoading = false,
		$address;

    function init() {
        $address = $("#map-address");

        if (window.location.hash === "#page-location") {
            initLocation();
        } else {
            $("#page-location").on("pageinit", initLocation);
        }

        //show loading mask in case the location not found yet 
        //and user returns to the page
        $("#page-location").on("pageshow", showPage);

        //hide loading mask when user leaves the page and it is only relevant to location
        $("#page-location").on("pagehide", hidePage);

        $("#map-navigate-home").on("click", navigateHome);
        $("#map-search").on("click", searchAddress);
    }

    $(document).on("deviceready", init);

    function initLocation() {
        var mapOptions = {
            zoom: 10,
            mapTypeId: google.maps.MapTypeId.ROADMAP,
            zoomControl: true,
            zoomControlOptions: {
                position: google.maps.ControlPosition.LEFT_BOTTOM
            },

            mapTypeControl: false,
            streetViewControl: false
        };

        map = new google.maps.Map(document.getElementById("map-canvas"), mapOptions);
        geocoder = new google.maps.Geocoder();
        navigateHome();
    }

    function navigateHome() {
        var position;

        isLoading = true;
        showPage();

        navigator.geolocation.getCurrentPosition(
			function (position) {
			    position = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
			    map.panTo(position);
			    loadMe(position);

			    isLoading = false;
			    hidePage();
			}, function (error) {
			    //default map coordinates
			    position = new google.maps.LatLng(55.6761, 12.5683);
			    map.panTo(position);
			    loadNearby(position);

			    isLoading = false;
			    hidePage();

			    navigator.notification.alert("Unable to determine current location. Cannot connect to GPS satellite.",
					function () { }, "Location failed", 'OK');
			}, {
			    timeout: 30000,
			    enableHighAccuracy: true
			});
    }

    function searchAddress() {
        var address = $address.val();

        geocoder.geocode(
			{
			    'address': address
			}, function (results, status) {
			    if (status !== google.maps.GeocoderStatus.OK) {
			        navigator.notification.alert("Unable to find address.",
						function () { }, "Search failed", 'OK');

			        return;
			    }

			    map.panTo(results[0].geometry.location);
			    loadMe(results[0].geometry.location);
			    loadNearby(results[0].geometry.location);
			});
    }

    function showPage() {
        if (isLoading) {
            $.mobile.loading("show");
        }

        google.maps.event.trigger(map, "resize");
    }
    function hidePage() {
        $.mobile.loading("hide");
    }

    function loadMe(position) {
        if (myLocation !== null && myLocation !== undefined) {
            myLocation.setMap(null);
        }

        myLocation = new google.maps.Marker({
            map: map,
            position: position,
            icon: {
                    path: google.maps.SymbolPath.CIRCLE,
                    scale: 7
                }
        });
    }

    function loadNearby(position) {
        function addInfoWindow(marker, message) {
            var info = '<a href="http://www.google.com">' + message + '</a>';

            var infoWindow = new google.maps.InfoWindow({
                content: info
            });

            google.maps.event.addListener(marker, 'click', function () {
                infoWindow.open(map, this);
            });
        }

        var nearbyPeople = [
            { name: 'Razvan', age: '20', gender: 'male', currentPosition: { latitude: '55.6761', longitude: '12.5610' } },
            { name: 'Paul', age: '21', gender: 'male', currentPosition: { latitude: '55.6761', longitude: '12.5620' } },
	        { name: 'Benja', age: '30', gender: 'male', currentPosition: { latitude: '55.6761', longitude: '12.5630' } },
            { name: 'Ruslan', age: '24', gender: 'male', currentPosition: { latitude: '55.6731', longitude: '12.5610' } },
            { name: 'Secret spy', age: '30', gender: 'female', currentPosition: { latitude: '55.6791', longitude: '12.5610' } }
        ];
        var markers = [];
        var infoWindows = [];
        for (var i = 0; i < nearbyPeople.length; i++) {
            markers[i] = new google.maps.Marker({
                map: map,
                position: new google.maps.LatLng(nearbyPeople[i].currentPosition.latitude, nearbyPeople[i].currentPosition.longitude)
            });
            addInfoWindow(markers[i], nearbyPeople[i].name + ', ' + nearbyPeople[i].gender + ', ' + nearbyPeople[i].age);
        }
        setAllMap(markers);
    }

    function setAllMap(markers) {
        for (var i = 0; i < markers.length; i++) {
            markers[i].setMap(map);
        }
    }

    $(function () {
        $("[data-role='navbar']").navbar();
    });
    // Update the contents of the toolbars
    $(document).on("pageshow", "[data-role='page']", function () {
        // Each of the four pages in this demo has a data-title attribute
        // which value is equal to the text of the nav button
        // For example, on first page: <div data-role="page" data-title="Info">
        var current = $(this).jqmData("title");
        // Change the heading
        $("[data-role='header'] h1").text(current);
        // Remove active class from nav buttons
        $("[data-role='navbar'] a.ui-btn-active").removeClass("ui-btn-active");
        // Add active class to current nav button
        $("[data-role='navbar'] a").each(function () {
            if ($(this).text() === current) {
                $(this).addClass("ui-btn-active");
            }
        });
    });

})(window);